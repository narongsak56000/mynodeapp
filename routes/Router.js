const express = require('express')
const router = express.Router()
// Use model
const Product = require('../models/product')

// Uer multer for upload file
const multer = require('multer')

const storage = multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'./public/images/products') // File location
    },
    filename:(req,file,cb)=>{
        cb(null,Date.now() + ".jpg") // Change file name
    }
})

// Start upload
const upload = multer({
    storage:storage
})

router.get('/',(req,res)=>{
    Product.find().exec((err,doc)=>{
        res.render('index',{products:doc})
    })
})

router.get('/addform',(req,res)=>{
    res.render('form')
})

router.get('/manage',(req,res)=>{
    Product.find().exec((err,doc)=>{
        res.render('manage',{products:doc})
    })
})

router.get('/delete/:id',(req,res)=>{
    Product.findByIdAndDelete(req.params.id,{useFindAndModify:false}).exec(err=>{
        if(err) console.log(err)
        res.redirect('/manage')
    })
})

router.post('/insert',upload.single("img"),(req,res)=>{
    let doc = new Product({
        name:req.body.name,
        price:req.body.price,
        image:req.file.filename,
        description:req.body.description,
    })
    Product.saveProduct(doc,(err)=>{
        if(err) console.log(err)
        res.redirect('/')
    })
})

// router.get('/', (req,res)=>{
//     res.status(200)
//     res.type('text/html')
//     res.sendFile(path.join(__dirname,'../templates/index.html'))
// })
// router.get('/product/:id', (req,res)=>{
//     const productID = req.params.id
//     if(productID === "1"){
//         res.sendFile(path.join(__dirname,'../templates/product1.html'))
//     }else if(productID === "2"){
//         res.sendFile(path.join(__dirname,'../templates/product2.html'))
//     }else if(productID === "3"){
//         res.sendFile(path.join(__dirname,'../templates/product3.html'))
//     }else{
//         res.redirect('/')
//     }
// })

module.exports = router