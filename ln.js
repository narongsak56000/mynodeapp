

const mymodules = require('./modules/mymodules.js')

console.log(mymodules.getCurrentTime())
console.log(mymodules.add(10,10))


const connect = true
const url1="dir.dev/file1.json"
const url2="dir.dev/file2.json"
const url3="dir.dev/file3.json"
const url4="dir.dev/file4.json"
const url5="dir.dev/file5.json"
function downloading(url){
  return new Promise(function(resolve,reject){
    console.log("Downloading...")
    setTimeout(()=>{
      if(connect){
        resolve(`${url} downloaded`)
      }else{
        reject('Errer')
      }
    },1000)
  })
};

async function start(){
  await downloading(url1)
  await downloading(url2)
  await downloading(url3)
  await downloading(url4)
  await downloading(url5)
}

start()

downloading(url1).then(function(result){
  console.log(result)
  return downloading(url2)
}).then(function(result){
  console.log(result)
  return downloading(url3)
}).then(function(result){
  console.log(result)
})

downloading(url1).then(function(result){
  console.log(result)
})

downloading(url1).then(result=>{
  console.log(result)
}).catch(err=>{
  console.log(err)
}).finally(()=>console.log("Ended"))