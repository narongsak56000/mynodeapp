const http = require('http')
const fs = require('fs')
const url = require('url')

const indexpage = fs.readFileSync(`${__dirname}/templates/index.html`,'utf-8')
const productpage1 = fs.readFileSync(`${__dirname}/templates/product1.html`)
const productpage2 = fs.readFileSync(`${__dirname}/templates/product2.html`)
const productpage3 = fs.readFileSync(`${__dirname}/templates/product3.html`)

const server = http.createServer((req, res)=>{

    const {pathname, query} = url.parse(req.url, true)
    const pathName = req.url
    if(pathname==="/" || pathname==="/index"){
        res.end(indexpage)
    }else if(pathname==="/product"){
        if(query.id === "1"){
            res.end(productpage1)
        }else if(query.id === "2"){
            res.end(productpage2)
        }else if(query.id === "3"){
            res.end(productpage3)
        }
    }else{
        res.writeHead(404)
        res.end("Not Found")
    }

})

server.listen(3000,()=>{
    console.log("Server started in port 3000")
})