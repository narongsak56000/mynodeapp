// Use mongoose
const mongoose = require('mongoose')

// Connect to MongoDB
const dbUrl = 'mongodb://localhost:27017/productDB'
mongoose.connect(dbUrl,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
}).catch(err=>console.log(err))

// Design Schema
let productSchema = mongoose.Schema({
    name:String,
    price:Number,
    image:String,
    description:String,
})

// Create Model
let Product = mongoose.model("products", productSchema)

// Export Model
module.exports = Product

// Save
module.exports.saveProduct=(model,doc)=>{
    model.save(doc)
}